package example.jsonrpc4j.springboot.impl;


import example.jsonrpc4j.springboot.api.MyService;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class MyServiceImpl implements MyService {

    private WebDriver driver;

    public  MyServiceImpl(){
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("androidPackage","com.snc.sample.webview");
        chromeOptions.setExperimentalOption("androidActivity", "com.snc.sample.webview.activity.WebViewActivity");
        chromeOptions.setExperimentalOption("androidUseRunningApp", true);
        driver = new ChromeDriver(chromeOptions);
    }

    public WebElement element(By by) {
        Wait wait = new FluentWait(driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(250, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        WebElement element;
        element = (WebElement) wait.until((Function<WebDriver, WebElement>) driver1 -> (WebElement) driver1.findElement(by));

        wait.until(ExpectedConditions.elementToBeClickable(by));
        return element;
    }


    @Override
    public String sayHelloWorld(String name) {
        return "Hello world, " + name;
    }

    @Override
    public String getURL(String name) {
        return driver.getCurrentUrl();
    }

    @Override
    public void clickByText(String identifier) {
        element(By.xpath("//*[text()='"+identifier+"']")).click();
    }

    @Override
    public boolean clickById(String identifier) {
        String script = "document.getElementById('"+identifier+"').click()";
        return actionOnElementByJS('#'+identifier,script);
    }

    @Override
    public boolean setTextById(String identifier, String value) {
        String script = "document.getElementById('"+identifier+"').value='"+value+"';";
        return actionOnElementByJS('#'+identifier,script);
    }

    @Override
    public boolean clickBySelector(String identifier, int index) {
        String script = "document.querySelectorAll('" + identifier + "')[0].click()";
        return actionOnElementByJS(identifier,script);
    }

    @Override
    public boolean setTextBySelector(String identifier, int index, String value) {
        String script = "document.querySelectorAll('"+identifier+"')["+index+".value='"+value+"';";
        return actionOnElementByJS(identifier,script); }

    private boolean actionOnElementByJS(String identifier, String script){
        int i = 0;
        while (i< 5) {
            try {
                if (element(By.cssSelector(identifier)).isDisplayed()) {
                    JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
                    javascriptExecutor.executeScript(script);
                    return true;
                } else {
                    System.out.println("Element not displayed");
                    return false;
                }
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
                try { // Replace with ScheduleExecutor
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
            }
            i++;
        }
        return false;
    }


}

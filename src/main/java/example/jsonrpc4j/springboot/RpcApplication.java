package example.jsonrpc4j.springboot;

import com.googlecode.jsonrpc4j.spring.JsonServiceExporter;
import example.jsonrpc4j.springboot.api.MyService;
import example.jsonrpc4j.springboot.impl.MyServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;

@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@SpringBootApplication
public class RpcApplication {

    public static void main(String[] args) {
        SpringApplication.run(RpcApplication.class);
    }

    @Bean
    public MyService myService() {
        return new MyServiceImpl();
    }

    @Bean(name = "/rpc/myservice")
    public JsonServiceExporter jsonServiceExporter() {
        JsonServiceExporter exporter = new JsonServiceExporter();
        exporter.setService(myService());
        exporter.setServiceInterface(MyService.class);
        return exporter;
    }
}

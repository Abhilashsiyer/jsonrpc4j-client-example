package example.jsonrpc4j.springboot.api;

import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;

import java.util.NoSuchElementException;

public interface MyService {

    final static int ERROR_CODE_BASE = -32000;

    String sayHelloWorld(String name);

    String getURL(String name);

    void clickByText(String name);

    @JsonRpcErrors({@JsonRpcError(exception= NoSuchElementException.class, message="retry", code=1)})
    boolean clickById(String identifier);

    @JsonRpcErrors({@JsonRpcError(exception= NoSuchElementException.class, message="retry", code=1)})
    boolean setTextById(String identifier, String value);

    @JsonRpcErrors({@JsonRpcError(exception= NoSuchElementException.class, message="retry", code=1)})
    boolean clickBySelector(String identifier,int index);

    @JsonRpcErrors({@JsonRpcError(exception= NoSuchElementException.class, message="retry", code=1)})
    boolean setTextBySelector(String identifier, int index, String value);
}
